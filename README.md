![cd1](media/logo.png?style=centerme "cd1") 

## Bienvenidos a nuestro querido Club de Informática

## ¿Quienes Somos?

Somos un pequeño grupo de estudiantes que quiso tener la iniciativa en crear un espacio alternativo para promover el desarrollo creativo/investigador de cada participante ligado al área de la informática.

Creemos en el entusiasmo de cada uno para motivar a realizar proyectos dependiendo de sus intereses.

Este club está pensado para que sea un espacio light y sin jerarquizaciones.

Como denominador común queremos compartir y conquistar conocimiento. Nada mas hermoso que enseñar, trabajar en equipo y aprender de ello.

## Código de Conducta:

1. No es necesario ser tan formal, pero tratar con respeto.

2. Usa las [Netiquetas](https://es.wikipedia.org/wiki/Netiqueta).

3. [Respeta el debate sin ofensas personales](https://www.youtube.com/watch?v=ESwDIXXyh_Y).

4. Abraza la camaradería real entre compañeros.

5. Declaramos el club como un espacio seguro: no incomodes a nadie; si pasa, hacete cargo y pedí disculpas.

6. No abuses del lugar ni de las cosas.

7. Las cosas que están en el club se quedan en el club, por favor no te lleves nada que no te pertenezca.

8. Si algo tiene marcado un nombre o está en un espacio destinado a alguien NO LO TOQUES. El resto es [hackeable](https://www.youtube.com/watch?v=zQ470q7z91k) bajo tu responsabilidad.

9. Tener en cuenta el [impacto social](https://t.me/valenzine/912) ([hacktivismo](https://www.youtube.com/watch?v=7jhdj0vKbYo)).

10. Comparte documentación oficial y contenido de calidad.

11. Incentivar a [salir de zona de confort](https://www.youtube.com/watch?v=RSUykLfEmVE)... salte de la caja.

12. Se aconseja uso de sistemas unix-like.

13. Estimula la instalación y uso de [GNU/Linux](http://www.youtube.com/watch?v=Cyi_vRff6Z8) y otros sistemas operativos libres.

14. Potencia el despliegue de servicios de red autoalojados y autogestionados. (opcional)

15. Difunde recursos, talleres y eventos.

16. Lucha por la educación y el conocimiento.

17. Somos defensores de la cultura, [hardware](https://es.wikipedia.org/wiki/Hardware_libre) y [software libre](https://es.wikipedia.org/wiki/Software_libre), por lo tanto toda herramienta que se desarrolle en el espacio tiene que ser libre.

18. Concientiza sobre el [anonimato y la privacidad](https://www.youtube.com/watch?v=NPE7i8wuupk) de los individuos en la red. (grupos internos)

19. Motiva la cultura del "hágalo ud. mismo". (cree usted la iniciativa y le damos acompañamiento)

20. El espacio designado para los servidores y la infraestructura es sagrado: no se puede comer ni beber cerca, ni apoyarse.

21. El club se mantiene con donaciones. Por favor contribuí con lo que puedas: equipos, muebles, dinero, etc.

22. Usamos la [adhocracia](https://es.wikipedia.org/wiki/Adhocracia), si ves que hay que hacer algo, hacelo.

23. **No se aceptan actividades de partidos políticos.**

## Código antiacoso:

El club es un espacio libre de acoso. Usamos como marco de referencia el código anti acoso de [geekFeminismi](https://geekfeminism.fandom.com/wiki/Geek_Feminism_Wiki).

Entre muchas otras consideramos como acoso a:

1. Comentarios ofensivos relacionados con el/los género/s, la identidad y expresión de género, la orientación sexual, las discapacidades, capacidades diferentes, neuroatipicidad, la apariencia física, el tamaño corporal, orientación política, la raza o la religión.

2. Comentarios que refuercen estructuras sociales de dominación.

3. Imágenes sexuales en espacios públicos sin previo consenso.

4. Seguir, acosar (stalkear) o intimidar deliberadamente a alguien.

5. Fotografiar o grabar a alguien sin su consentimiento.

6. Interrumpir continuamente charlas o eventos.

7. Contacto físico inapropiado.

8. Atención sexual indeseada.

9. Promover o fomentar cualquiera de las anteriores.

Si ves que alguien está siendo acosada/o por favor seguir el protocolo de conflictos interno.

Si estas siendo acosada, por favor primero pedile que pare; si no lo hace, pedile que se vaya y buscá a una persona de confianza que te pueda ayudar.

*Licencia y atribuciones*

*Este código de conducta esta licenciado bajo CC BY-NC-SA 4.0.*

*Es una traducción y adaptación al contexto nacional de la Anti-harassment policy de Geek Feminism.*

## Contacto:

Contactate con [@Wongia](http://t.me/Wongia), [@ElBrujas](http://t.me/ElBrujas) o [@Mad_Scientist_001](http://t.me/Mad_Scientist_001) para pedir el link del grupo.
