# Organización

Nos organizamos de la siguiente manera:

1) Si bien nos manejamos por adhocracia: Tenemos dos representantes encargados para la gestión del club.
2) Se debe tener un Proyecto o bien integrarse a un Proyecto.
3) Cada Proyecto debe presentarse con un [Anteproyecto](https://www.significados.com/anteproyecto/). (el modelo del anteproyecto se subirá al repositorio cuando esté terminada). 
4) Cada Proyecto debe estar documentado.
5) Todos los proyectos los subimos a [GitLab](https://gitlab.com/) (solo documentación y presentación), pero cada participante puede gestionarlo por otro medio.
6) Nos comunicamos por Telegram.
7) Nuestro medio de comunicación al público puede variar. Y solo será para ese propósito: comunicar
8) Nuestros medios de transmisión serán por [Jitsi Meet](https://jitsi.org/), [FediverseTV](https://fediverse.tv/) y en última instancia se hará por Google Meet. No transmitimos por: Twitch, Zoom, Adobe Connect.
